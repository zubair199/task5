package com.example.task5;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainAcitivity extends AppCompatActivity {
    String[] team_name = {"Islamabad United","Karachi Kings","Lahore Qalander","Multan Sultan","Peshawar Zalmi","Quetta Gladiators"};
    int[] team_images ={R.drawable.islamabad_united,R.drawable.karachi_kings,R.drawable.lahore_qalanders,R.drawable.multan_sultan,R.drawable.peshawar_zalmi,R.drawable.quetta_gladiators};
    ListView l1;
    Adapter adp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.row);

        l1 = (ListView) findViewById(R.id.List1);

        adp = new Adapter(MainAcitivity.this, team_name, team_images);

        l1.setAdapter(adp);



    }


}
