package com.example.task5;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.BaseAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.view.View;
public class Adapter extends BaseAdapter {
    Context context;
    private final String[] teams;
    private final int[] flags;

    public Adapter(Context context, String[] arg1, int[] arg2)
    {
        this.context = context;
        teams = arg1;
        flags = arg2;
    }
    @Override
    public int getCount() {
        return teams.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public View getView(int position,  View convertView,  ViewGroup parent) {
        ViewHolder viewHolder;
        final View result;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.single_view, parent, false);
            viewHolder.images = (ImageView) convertView.findViewById(R.id.Flag);
            viewHolder.text1= (TextView) convertView.findViewById(R.id.Team);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        viewHolder.text1.setText(teams[position]);
        viewHolder.images.setImageResource(flags[position]);
        return convertView;
    }
    private static class ViewHolder {
        TextView text1;
        ImageView images;
    }

}
